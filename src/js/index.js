const button = document.querySelector(".button");
const buttonChange = document.querySelector(".button__span");
const navigator = document.querySelector(".navigator");
const navHoverElement = document.querySelector(".navigator__list");
const navItem = document.querySelectorAll(".navigator__item");

button.addEventListener("click", function(e){
    e.preventDefault();
    buttonChange.classList.toggle("active");
    navigator.classList.toggle("show");
    navItem.forEach(item =>{
        item.style.backgroundColor = "#F5F5F5"
    });
});

navHoverElement.addEventListener("click", function(e){
    if(window.matchMedia('(max-width: 768px)').matches){
        navItem.forEach(item =>{
            item.style.backgroundColor = "#F5F5F5"
        });
        e.target.closest("li").style.backgroundColor = "#E5E5E5";
    }})

